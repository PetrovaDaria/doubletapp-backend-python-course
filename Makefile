build:
	docker-compose build

push:
	docker-compose push ${IMAGE_APP}

pull:
	docker-compose pull ${IMAGE_APP}

down:
	docker-compose down

up:
	docker-compose up -d

migrate_ci:
	docker-compose run app python src/manage.py makemigrations app
	docker-compose run app python src/manage.py migrate

migrate:
	python src/manage.py migrate $(if $m, api $m,)

makemigrations:
	python src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

createsuperuser:
	python src/manage.py createsuperuser

collectstatic:
	python src/manage.py collectstatic --no-input

dev:
	docker-compose -f docker-compose.local.yml up --build

run:
	python src/manage.py runserver 0.0.0.0:8000

command:
	python src/manage.py ${c}

shell:
	python src/manage.py shell

debug:
	python src/manage.py debug

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .

check_lint_ci:
	docker-compose run app isort --check --diff .
	docker-compose run app flake8 --config setup.cfg
	docker-compose run app black --check --config pyproject.toml .
