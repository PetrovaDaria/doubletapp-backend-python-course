from django.contrib import admin

from app.internal.admin.admin_user import AdminUserAdmin
from app.internal.admin.bank_account_adminka import BankAccountAdminka
from app.internal.admin.bank_card_adminka import BankCardAdminka
from app.internal.admin.telegram_user_adminka import TelegramUserAdminka

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
