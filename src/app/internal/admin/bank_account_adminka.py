from django.contrib import admin

from app.internal.models.bank_account import BankAccount


@admin.register(BankAccount)
class BankAccountAdminka(admin.ModelAdmin):
    fields = ["number", "balance", "currency"]
