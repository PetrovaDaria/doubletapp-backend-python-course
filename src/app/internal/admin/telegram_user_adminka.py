from django.contrib import admin

from app.internal.models.telegram_user import TelegramUser


@admin.register(TelegramUser)
class TelegramUserAdminka(admin.ModelAdmin):
    fields = ["telegram_id", "telegram_login", "name", "phone_number"]
