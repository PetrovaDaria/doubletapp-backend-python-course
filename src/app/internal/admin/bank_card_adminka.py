from django.contrib import admin

from app.internal.models.bank_card import BankCard


@admin.register(BankCard)
class BankCardAdminka(admin.ModelAdmin):
    fields = ["number", "account_id", "owner_id"]
