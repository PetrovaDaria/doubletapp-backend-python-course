from app.internal.models.bank_card import BankCard
from app.internal.models.telegram_user import TelegramUser


class GetUserCardsBalanceService:
    def execute(self, user: TelegramUser):
        user_cards = BankCard.objects.filter(owner_id=user.id)
        info = []
        for card in user_cards:
            card_info = f"Карта №{card.number}: {card.account_id.balance} {card.account_id.currency}"
            info.append(card_info)
        info_str = "\n".join(info)
        return info_str
