from app.internal.models.telegram_user import TelegramUser


class IsTelegramUserExistsByIdService:
    def execute(self, telegram_id):
        return TelegramUser.objects.filter(telegram_id=telegram_id).exists()
