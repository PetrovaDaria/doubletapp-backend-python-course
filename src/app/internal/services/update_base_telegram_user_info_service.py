from app.internal.models.telegram_user import TelegramUser


class UpdateBaseTelegramUserInfoService:
    def execute(self, user: TelegramUser, new_name, new_login):
        user.name = new_name
        user.telegram_login = new_login
        user.save(update_fields=["telegram_login", "name"])
