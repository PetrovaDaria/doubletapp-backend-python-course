from app.internal.models.telegram_user import TelegramUser


class GetTelegramUserByIdService:
    def execute(self, telegram_id):
        return TelegramUser.objects.get(telegram_id=telegram_id)
