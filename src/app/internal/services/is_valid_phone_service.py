import re


class IsValidPhoneService:
    phone_regex = r"^(\+?[0-9]{11})|(\+?\d{1}[ -]?\(?\d{3}\)?[ -]?\d{3}[ -]?\d{2}[ -]?\d{2})$"

    def execute(self, phone):
        match = re.match(self.phone_regex, phone)
        return match is not None
