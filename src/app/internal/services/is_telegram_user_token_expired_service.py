import datetime

from django.utils.timezone import make_aware


class IsTelegramUserTokenExpiredService:
    def execute(self, token_created_at):
        naive_now = datetime.datetime.now()
        aware_now = make_aware(naive_now)
        token_created_at_plus_day = token_created_at + datetime.timedelta(days=1)
        return aware_now > token_created_at_plus_day
