from app.internal.models.telegram_user import TelegramUser


class IsTelegramUserHasPhoneService:
    def execute(self, telegram_user: TelegramUser):
        return telegram_user.phone_number != ""
