from app.internal.models.telegram_user import TelegramUser


class CreateOrUpdateTelegramUserService:
    def execute(self, telegram_id, name, login):
        TelegramUser.objects.create(telegram_id=telegram_id, name=name, telegram_login=login)
