class GetAccountsBalanceHandlerService:
    def __init__(self, get_user_by_id_service, get_user_accounts_balance_service):
        self.get_user_by_id_service = get_user_by_id_service
        self.get_user_accounts_balance_service = get_user_accounts_balance_service

    def execute(self, telegram_id):
        # TODO: дописать проверки на существование пользователя
        user = self.get_user_by_id_service.execute(telegram_id)
        return self.get_user_accounts_balance_service.execute(user)
