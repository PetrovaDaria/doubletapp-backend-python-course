SET_PHONE_HANDLER_NO_USER_RESULT = "no_user"
SET_PHONE_HANDLER_NOT_VALID_PHONE_RESULT = "not_valid_phone"
SET_PHONE_HANDLER_SUCCESS_RESULT = "set"


class SetPhoneHandlerService:
    def __init__(self, is_user_exists_service, is_valid_phone_service, set_phone_service):
        self.is_user_exists_service = is_user_exists_service
        self.is_valid_phone_service = is_valid_phone_service
        self.set_phone_service = set_phone_service

    def execute(self, telegram_id, phone):
        is_user_exists = self.is_user_exists_service.execute(telegram_id)
        if not is_user_exists:
            return SET_PHONE_HANDLER_NO_USER_RESULT

        is_valid_phone = self.is_valid_phone_service.execute(phone)
        if not is_valid_phone:
            return SET_PHONE_HANDLER_NOT_VALID_PHONE_RESULT

        self.set_phone_service.execute(telegram_id, phone)
        return SET_PHONE_HANDLER_SUCCESS_RESULT
