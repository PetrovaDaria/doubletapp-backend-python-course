ME_HANDLER_NO_USER_RESULT = "no_user"
ME_HANDLER_NO_PHONE_RESULT = "no_phone"


class MeHandlerService:
    def __init__(
        self, is_user_exists_service, is_user_has_phone_service, get_user_service, update_base_user_info_service
    ):
        self.is_user_exists_service = is_user_exists_service
        self.is_user_has_phone_service = is_user_has_phone_service
        self.get_user_service = get_user_service
        self.update_base_user_info_service = update_base_user_info_service

    def execute(self, telegram_id, name, login):
        is_user_exists = self.is_user_exists_service.execute(telegram_id)
        if not is_user_exists:
            return ME_HANDLER_NO_USER_RESULT

        telegram_user = self.get_user_service.execute(telegram_id)
        is_phone_exists = self.is_user_has_phone_service.execute(telegram_user)
        if not is_phone_exists:
            return ME_HANDLER_NO_PHONE_RESULT

        self.update_base_user_info_service.execute(telegram_user, name, login)
        return {"name": telegram_user.name, "login": telegram_user.telegram_login, "phone": telegram_user.phone_number}
