GET_TOKEN_HANDLER_NO_USER_RESULT = "no_user"
GET_TOKEN_HANDLER_NO_PHONE_RESULT = "no_phone"


class GetTokenHandlerService:
    def __init__(
        self,
        is_user_exists_service,
        is_user_has_phone_service,
        get_user_service,
        create_or_update_token_service,
        is_token_expired_service,
        get_token_expiration_info_service,
    ):
        self.is_user_exists_service = is_user_exists_service
        self.is_user_has_phone_service = is_user_has_phone_service
        self.get_user_service = get_user_service
        self.create_or_update_token_service = create_or_update_token_service
        self.is_token_expired_service = is_token_expired_service
        self.get_token_expiration_info_service = get_token_expiration_info_service

    def execute(self, telegram_id):
        is_user_exists = self.is_user_exists_service.execute(telegram_id)
        if not is_user_exists:
            return GET_TOKEN_HANDLER_NO_USER_RESULT

        telegram_user = self.get_user_service.execute(telegram_id)
        is_phone_exists = self.is_user_has_phone_service.execute(telegram_user)
        if not is_phone_exists:
            return GET_TOKEN_HANDLER_NO_PHONE_RESULT

        if telegram_user.me_token is None:
            self.create_or_update_token_service.execute(telegram_user)
        else:
            is_token_expired = self.is_token_expired_service.execute(telegram_user.me_token_created_at)
            if is_token_expired:
                self.create_or_update_token_service.execute(telegram_user)
        token_expiration_info = self.get_token_expiration_info_service.execute(telegram_user)
        return {
            "token": telegram_user.me_token,
            "seconds": token_expiration_info["seconds"],
            "minutes": token_expiration_info["minutes"],
            "hours": token_expiration_info["hours"],
        }
