START_HANDLER_CREATE_RESULT = "create"
START_HANDLER_UPDATE_RESULT = "update"


class StartHandlerService:
    def __init__(
        self, is_user_exists_service, create_user_service, get_telegram_user_service, update_base_user_info_service
    ):
        self.is_user_exists_service = is_user_exists_service
        self.create_user_service = create_user_service
        self.get_telegram_user_service = get_telegram_user_service
        self.update_base_user_info_service = update_base_user_info_service

    def execute(self, telegram_id, name, login):
        is_user_exists = self.is_user_exists_service.execute(telegram_id)

        if not is_user_exists:
            self.create_user_service.execute(telegram_id, name, login)
            return START_HANDLER_CREATE_RESULT

        telegram_user = self.get_telegram_user_service.execute(telegram_id)
        self.update_base_user_info_service.execute(telegram_user, name, login)
        return START_HANDLER_UPDATE_RESULT
