import datetime

from django.utils.timezone import make_aware

from app.internal.models.telegram_user import TelegramUser

seconds_in_hour = 60 * 60
seconds_in_minute = 60


class GetTokenExpirationInfoService:
    def execute(self, telegram_user: TelegramUser):
        token_created_at_plus_day = telegram_user.me_token_created_at + datetime.timedelta(days=1)
        naive_now = datetime.datetime.now()
        aware_now = make_aware(naive_now)
        before_expiration_delta = token_created_at_plus_day - aware_now
        all_before_expiration_seconds = before_expiration_delta.seconds
        before_expiration_hours = all_before_expiration_seconds // seconds_in_hour
        before_expiration_minutes = (
            all_before_expiration_seconds - before_expiration_hours * seconds_in_hour
        ) // seconds_in_minute
        before_expiration_seconds = (
            all_before_expiration_seconds
            - before_expiration_hours * seconds_in_hour
            - before_expiration_minutes * seconds_in_minute
        )
        return {
            "hours": before_expiration_hours,
            "minutes": before_expiration_minutes,
            "seconds": before_expiration_seconds,
        }
