from app.internal.models.telegram_user import TelegramUser


class SetTelegramUserPhoneService:
    def _prettify_phone(self, phone):
        return phone.replace(r"[^0-9+]", "")

    def execute(self, telegram_id, phone: str):
        prettified_phone = self._prettify_phone(phone)
        telegram_user = TelegramUser.objects.get(telegram_id=telegram_id)
        telegram_user.phone_number = prettified_phone
        telegram_user.save()
