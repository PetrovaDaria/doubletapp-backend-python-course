import datetime
from uuid import uuid4

from django.utils.timezone import make_aware

from app.internal.models.telegram_user import TelegramUser


class CreateOrUpdateTelegramUserTokenService:
    def execute(self, telegram_user: TelegramUser):
        generated_token = uuid4()
        telegram_user.me_token = generated_token
        naive_now = datetime.datetime.now()
        aware_now = make_aware(naive_now)
        telegram_user.me_token_created_at = aware_now
        telegram_user.save()
