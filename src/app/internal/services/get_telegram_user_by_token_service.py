from app.internal.models.telegram_user import TelegramUser


class GetTelegramUserByTokenService:
    def execute(self, token):
        return TelegramUser.objects.get(me_token=token)
