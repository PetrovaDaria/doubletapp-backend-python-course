HTTP_ME_HANDLER_NO_USER_RESULT = "no_user"


class HttpMeHandlerService:
    def __init__(self, is_user_exists_service, get_user_service):
        self.is_user_exists_service = is_user_exists_service
        self.get_user_service = get_user_service

    def execute(self, token):
        is_user_exists = self.is_user_exists_service.execute(token)
        if not is_user_exists:
            return HTTP_ME_HANDLER_NO_USER_RESULT

        telegram_user = self.get_user_service.execute(token)
        user_info_json = {
            "name": telegram_user.name,
            "login": telegram_user.telegram_login,
            "phone": telegram_user.phone_number,
        }
        return user_info_json
