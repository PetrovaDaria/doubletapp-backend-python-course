from app.internal.models.telegram_user import TelegramUser


class IsTelegramUserExistsByTokenService:
    def execute(self, token):
        return TelegramUser.objects.filter(me_token=token).exists()
