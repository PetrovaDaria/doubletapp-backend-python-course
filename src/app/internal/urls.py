from django.urls import path

from app.internal.transport.rest.handlers import MeView

urlpatterns = [path("me/", MeView.as_view(), name="me")]
