from telegram.ext import Dispatcher, Updater

from app.internal.transport.bot.handlers import (
    get_accounts_balance_handler,
    get_cards_balance_handler,
    get_token_handler,
    me_handler,
    set_phone_handler,
    start_handler,
)
from config.settings import TELEGRAM_BOT_TOKEN


def start_telegram_bot():
    updater = Updater(token=TELEGRAM_BOT_TOKEN, use_context=True)
    dispatcher: Dispatcher = updater.dispatcher
    dispatcher.add_handler(start_handler)
    dispatcher.add_handler(set_phone_handler)
    dispatcher.add_handler(me_handler)
    dispatcher.add_handler(get_token_handler)
    dispatcher.add_handler(get_accounts_balance_handler)
    dispatcher.add_handler(get_cards_balance_handler)
    updater.start_polling()


def run_bot():
    start_telegram_bot()
