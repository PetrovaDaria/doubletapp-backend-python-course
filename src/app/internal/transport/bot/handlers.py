from telegram import MessageEntity, Update, constants
from telegram.ext import CallbackContext, CommandHandler

from app.internal.services.bot_handlers import START_HANDLER_UPDATE_RESULT, StartHandlerService
from app.internal.services.bot_handlers.get_accounts_balance_handler_service import GetAccountsBalanceHandlerService
from app.internal.services.bot_handlers.get_cards_balance_handler_service import GetCardsBalanceHandlerService
from app.internal.services.bot_handlers.get_token_handler_service import (
    GET_TOKEN_HANDLER_NO_PHONE_RESULT,
    GET_TOKEN_HANDLER_NO_USER_RESULT,
    GetTokenHandlerService,
)
from app.internal.services.bot_handlers.me_handler_service import (
    ME_HANDLER_NO_PHONE_RESULT,
    ME_HANDLER_NO_USER_RESULT,
    MeHandlerService,
)
from app.internal.services.bot_handlers.set_phone_handler_service import (
    SET_PHONE_HANDLER_NO_USER_RESULT,
    SET_PHONE_HANDLER_NOT_VALID_PHONE_RESULT,
    SET_PHONE_HANDLER_SUCCESS_RESULT,
    SetPhoneHandlerService,
)
from app.internal.services.create_new_telegram_user_service import CreateOrUpdateTelegramUserService
from app.internal.services.create_or_update_telegram_user_token_service import CreateOrUpdateTelegramUserTokenService
from app.internal.services.get_telegram_user_by_id_service import GetTelegramUserByIdService
from app.internal.services.get_token_expiration_info_service import GetTokenExpirationInfoService
from app.internal.services.get_user_accounts_balance_service import GetUserAccountsBalanceService
from app.internal.services.get_user_cards_balance_service import GetUserCardsBalanceService
from app.internal.services.is_telegram_user_exists_by_id_service import IsTelegramUserExistsByIdService
from app.internal.services.is_telegram_user_has_phone_service import IsTelegramUserHasPhoneService
from app.internal.services.is_telegram_user_token_expired_service import IsTelegramUserTokenExpiredService
from app.internal.services.is_valid_phone_service import IsValidPhoneService
from app.internal.services.set_telegram_user_phone_service import SetTelegramUserPhoneService
from app.internal.services.update_base_telegram_user_info_service import UpdateBaseTelegramUserInfoService
from app.internal.transport.bot.messages import (
    AUTH_TOKEN_EXPLANATION_MESSAGE,
    ENTER_VALID_PHONE_MESSAGE,
    FIRST_REGISTER_THAN_ME_MESSAGE,
    FIRST_REGISTER_THAN_SET_PHONE_MESSAGE,
    FIRST_REGISTER_THAN_TOKEN_MESSAGE,
    FIRST_SET_PHONE_THAN_ME_MESSAGE,
    FIRST_SET_PHONE_THAN_TOKEN_MESSAGE,
    PHONE_SAVED_MESSAGE,
    SAVE_INFORMATION_MESSAGE,
    UPDATE_INFORMATION_MESSAGE,
    get_before_expiration_left_message,
    get_user_info_message,
)


def start(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    telegram_id = update.effective_user.id
    name = update.effective_user.full_name
    login = update.effective_user.username

    is_user_exists_service = IsTelegramUserExistsByIdService()
    create_user_service = CreateOrUpdateTelegramUserService()
    get_telegram_user_service = GetTelegramUserByIdService()
    update_base_user_info_service = UpdateBaseTelegramUserInfoService()
    start_handler_service = StartHandlerService(
        is_user_exists_service, create_user_service, get_telegram_user_service, update_base_user_info_service
    )

    result = start_handler_service.execute(telegram_id, name, login)
    message = UPDATE_INFORMATION_MESSAGE if result == START_HANDLER_UPDATE_RESULT else SAVE_INFORMATION_MESSAGE
    context.bot.send_message(text=message, chat_id=chat_id)


def set_phone(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    telegram_id = update.effective_user.id
    raw_phone = " ".join(context.args)

    is_user_exists_service = IsTelegramUserExistsByIdService()
    is_valid_phone_service = IsValidPhoneService()
    set_phone_service = SetTelegramUserPhoneService()
    set_phone_handler_service = SetPhoneHandlerService(
        is_user_exists_service, is_valid_phone_service, set_phone_service
    )

    result = set_phone_handler_service.execute(telegram_id, raw_phone)
    message = ""
    if result == SET_PHONE_HANDLER_NO_USER_RESULT:
        message = FIRST_REGISTER_THAN_SET_PHONE_MESSAGE
    if result == SET_PHONE_HANDLER_NOT_VALID_PHONE_RESULT:
        message = ENTER_VALID_PHONE_MESSAGE
    if result == SET_PHONE_HANDLER_SUCCESS_RESULT:
        message = PHONE_SAVED_MESSAGE
    context.bot.send_message(text=message, chat_id=chat_id)


def me(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    telegram_id = update.effective_user.id
    name = update.effective_user.full_name
    login = update.effective_user.username

    is_user_exists_service = IsTelegramUserExistsByIdService()
    is_user_has_phone_service = IsTelegramUserHasPhoneService()
    get_user_service = GetTelegramUserByIdService()
    update_base_user_info_service = UpdateBaseTelegramUserInfoService()
    me_handler_service = MeHandlerService(
        is_user_exists_service, is_user_has_phone_service, get_user_service, update_base_user_info_service
    )

    result = me_handler_service.execute(telegram_id, name, login)
    if result == ME_HANDLER_NO_USER_RESULT:
        message = FIRST_REGISTER_THAN_ME_MESSAGE
    elif result == ME_HANDLER_NO_PHONE_RESULT:
        message = FIRST_SET_PHONE_THAN_ME_MESSAGE
    else:
        message = get_user_info_message(result["name"], result["login"], result["phone"])
    context.bot.send_message(text=message, chat_id=chat_id)


def get_auth_token(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    telegram_id = update.effective_user.id

    is_user_exists_service = IsTelegramUserExistsByIdService()
    is_user_has_phone_service = IsTelegramUserHasPhoneService()
    get_user_service = GetTelegramUserByIdService()
    create_or_update_token_service = CreateOrUpdateTelegramUserTokenService()
    is_token_expired_service = IsTelegramUserTokenExpiredService()
    get_token_expiration_info_service = GetTokenExpirationInfoService()
    get_token_handler_service = GetTokenHandlerService(
        is_user_exists_service,
        is_user_has_phone_service,
        get_user_service,
        create_or_update_token_service,
        is_token_expired_service,
        get_token_expiration_info_service,
    )
    result = get_token_handler_service.execute(telegram_id)
    if result == GET_TOKEN_HANDLER_NO_PHONE_RESULT or result == GET_TOKEN_HANDLER_NO_USER_RESULT:
        message = ""
        if result == GET_TOKEN_HANDLER_NO_USER_RESULT:
            message = FIRST_REGISTER_THAN_TOKEN_MESSAGE
        elif result == GET_TOKEN_HANDLER_NO_PHONE_RESULT:
            message = FIRST_SET_PHONE_THAN_TOKEN_MESSAGE
        context.bot.send_message(text=message, chat_id=chat_id)
        return

    token = str(result["token"])
    before_expiration_message = get_before_expiration_left_message(
        result["hours"], result["minutes"], result["seconds"]
    )
    token_spoiler_entity = MessageEntity(type=constants.MESSAGEENTITY_SPOILER, length=len(token), offset=0)
    context.bot.send_message(entities=[token_spoiler_entity], text=token, chat_id=chat_id)
    context.bot.send_message(text=AUTH_TOKEN_EXPLANATION_MESSAGE, chat_id=chat_id)
    context.bot.send_message(text=before_expiration_message, chat_id=chat_id)


def get_accounts_balance(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    telegram_id = update.effective_user.id

    get_user_by_id_service = GetTelegramUserByIdService()
    get_user_accounts_balance_service = GetUserAccountsBalanceService()
    get_accounts_balance_handler_service = GetAccountsBalanceHandlerService(
        get_user_by_id_service, get_user_accounts_balance_service
    )
    accounts_info = get_accounts_balance_handler_service.execute(telegram_id)

    context.bot.send_message(text=accounts_info, chat_id=chat_id)


def get_cards_balance(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    telegram_id = update.effective_user.id

    get_user_by_id_service = GetTelegramUserByIdService()
    get_user_cards_balance_service = GetUserCardsBalanceService()
    get_cards_balance_handler_service = GetCardsBalanceHandlerService(
        get_user_by_id_service, get_user_cards_balance_service
    )
    cards_info = get_cards_balance_handler_service.execute(telegram_id)

    context.bot.send_message(text=cards_info, chat_id=chat_id)


start_handler = CommandHandler("start", start)
set_phone_handler = CommandHandler("set_phone", set_phone)
me_handler = CommandHandler("me", me)
get_token_handler = CommandHandler("get_token", get_auth_token)
get_accounts_balance_handler = CommandHandler("get_accounts_balance", get_accounts_balance)
get_cards_balance_handler = CommandHandler("get_cards_balance", get_cards_balance)
