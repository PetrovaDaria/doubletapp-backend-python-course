SAVE_INFORMATION_MESSAGE = "Мы сохранили о тебе информацию"
UPDATE_INFORMATION_MESSAGE = "Мы обновили о тебе информацию"
FIRST_REGISTER_THAN_SET_PHONE_MESSAGE = (
    "Пожалуйста сначала зарегистрируйся в системе с помощью команды /start. После этого ты сможешь сохранить телефон."
)
ENTER_VALID_PHONE_MESSAGE = (
    "Пожалуйста введи номер телефона в формате +71234567890, можно с скобками или пробелами в приемлимых местах."
)
PHONE_SAVED_MESSAGE = "Телефон сохранён"
FIRST_REGISTER_THAN_TOKEN_MESSAGE = "Пожалуйста сначала зарегистрируйся в системе с помощью команды /start и укажи телефон с помощью команды /set_phone. После этого ты сможешь получить авторизационный токен."
FIRST_SET_PHONE_THAN_TOKEN_MESSAGE = "Пожалуйста сначала укажи телефон с помощью команды /set_phone. После этого ты сможешь получить авторизационный токен."
FIRST_REGISTER_THAN_ME_MESSAGE = "Пожалуйста сначала зарегистрируйся в системе с помощью команды /start и укажи телефон с помощью команды /set_phone. После этого ты сможешь получить информацию о себе."
FIRST_SET_PHONE_THAN_ME_MESSAGE = (
    "Пожалуйста сначала укажи телефон с помощью команды /set_phone. После этого ты сможешь получить информацию о себе."
)
AUTH_TOKEN_EXPLANATION_MESSAGE = "Это твой авторизационный токен. Используй его для получения информации о себе по http. Просто добавь в запрос /api/me заголовок Authorization со значением Bearer {твой токен}"


def get_user_info_message(name, login, phone):
    return f"Имя: {name}. Логин: {login}. Телефон: {phone}"


def get_before_expiration_left_message(hours, minutes, seconds):
    return f"До протухания токена осталось {hours} ч. {minutes} мин. {seconds} сек."
