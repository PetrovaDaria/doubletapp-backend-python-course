from django.http import HttpRequest, HttpResponse, JsonResponse
from django.views import View

from app.internal.services.get_telegram_user_by_token_service import GetTelegramUserByTokenService
from app.internal.services.http_handlers.http_me_handler_service import (
    HTTP_ME_HANDLER_NO_USER_RESULT,
    HttpMeHandlerService,
)
from app.internal.services.is_telegram_user_exists_by_token_service import IsTelegramUserExistsByTokenService


class MeView(View):
    def get(self, request: HttpRequest):
        is_user_exists_service = IsTelegramUserExistsByTokenService()
        get_user_service = GetTelegramUserByTokenService()
        me_handler_service = HttpMeHandlerService(is_user_exists_service, get_user_service)

        try:
            token = request.headers["Authorization"][7:]
        except:
            return HttpResponse("Unauthorized", status=401)
        result = me_handler_service.execute(token)
        if result == HTTP_ME_HANDLER_NO_USER_RESULT:
            return HttpResponse("Unauthorized", status=401)
        else:
            return JsonResponse(result)
