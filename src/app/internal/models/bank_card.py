from django.core.validators import RegexValidator
from django.db import models

from app.internal.models.bank_account import BankAccount
from app.internal.models.telegram_user import TelegramUser


class BankCard(models.Model):
    number_regex = RegexValidator(regex=r"\d{16}", message="Number should consist of 16 digits")
    account_id = models.ForeignKey(BankAccount, on_delete=models.CASCADE)
    owner_id = models.ForeignKey(TelegramUser, on_delete=models.CASCADE)
    number = models.CharField(max_length=16, validators=[number_regex])
