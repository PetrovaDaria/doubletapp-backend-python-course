from django.core.validators import RegexValidator
from django.db import models


class TelegramUser(models.Model):
    telegram_id = models.CharField(max_length=255)
    telegram_login = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    phone_regex = RegexValidator(
        regex=r"^\+?1?\d{10}$",
        message="Phone number must be entered in the format: '+71234567890'. Up to 15 digits allowed.",
    )
    phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)  # validators should be a list
    me_token = models.CharField(max_length=255, null=True)
    me_token_created_at = models.DateTimeField(null=True)

    class Meta:
        verbose_name_plural = "Telegram Users"
