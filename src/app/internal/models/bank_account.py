from django.core.validators import RegexValidator
from django.db import models

RUB = "RUB"
EUR = "EUR"
USD = "USD"

CURRENCY_CHOICES = [("RUB", "Rubles"), ("EUR", "Euro"), ("USD", "Dollars")]


class BankAccount(models.Model):
    number_regex = RegexValidator(regex=r"\d{20}", message="Number should consist of 20 digits")
    number = models.CharField(max_length=20, validators=[number_regex])
    balance = models.IntegerField()
    currency = models.CharField(max_length=100, choices=CURRENCY_CHOICES, default=RUB)
