FROM python:3.9-slim-buster

ENV PYTHONUNBUFFERED=1

WORKDIR /usr/src/app

COPY Pipfile ./
COPY Pipfile.lock ./

RUN pip install pipenv && \
    pipenv install --system --deploy

EXPOSE 8000

COPY . .
